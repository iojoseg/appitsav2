package mx.tecnm.misantla.appitsav2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.tecnm.misantla.appitsav2.databinding.ActivityTerceraBinding

class TerceraActivity : AppCompatActivity() {
   lateinit var binding: ActivityTerceraBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTerceraBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle:Bundle? = intent.extras
        bundle?.let {
            val edad = it.getInt("dato1")
             binding.tvTerceraTexto.text = "La edad de su perro es ${edad} años"
        }
    }
}