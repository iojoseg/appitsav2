package mx.tecnm.misantla.appitsav2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import mx.tecnm.misantla.appitsav2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCalcular.setOnClickListener {
          val edad = binding.edtEdad.text.toString()

            if(!edad.isEmpty()){
                val edadint = edad.toInt()
                val resultado = edadint * 7
                Toast.makeText(this,
                    "La edad de su perro es ${resultado} años",
                    Toast.LENGTH_SHORT).show()
                binding.tvMainMsj.text = "La edad de su perro es ${resultado} años"

            }else{
                Toast.makeText(this,
                    "Debe ingresar una edad",
                    Toast.LENGTH_LONG).show()
            }
        }
        binding.btnEnviar.setOnClickListener {
            val edad = binding.edtEdad.text.toString().toInt()
            val resultado = edad * 7
            pasar_valores(resultado)
        }
    }

    private fun pasar_valores(valor:Int) {
        val bundle = Bundle()
        bundle.apply {
            putInt("dato1",valor)
        }
        val intent_main = Intent(this,TerceraActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent_main)

    }
}